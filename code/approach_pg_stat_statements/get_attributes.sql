-------------------------------------------------------
/**
 * Author:		Kevin Ammann
 * Created:		12.05.2021
 * Project:		Functional Dependency Detection with PL/pgSQL
 * Version:		Initial Version
 * Function:	Purpose: Parse queries from pg_stat_statements and return the query_id,table_name and attribute names
				Parameter: Max execution time in sec
**/
-------------------------------------------------------

CREATE OR REPLACE FUNCTION public.get_attributes(IN min_exec numeric DEFAULT NULL)
    RETURNS text[]
    LANGUAGE 'plpgsql'

    
AS $BODY$
DECLARE	
queries text[];
all_tables text[];
all_attributes text[];
q text;
tab text;
attr text;
condition_clause text;

query_in_output text;
potential_fd text[];
output_string text[];
add_potential_fd boolean;
tab_attr_list text[];
i_min_exec integer := coalesce(min_exec,0);
query_id text;

BEGIN

	queries := ARRAY(SELECT pgstats.queryid||','||regexp_replace(query,'[[:space:]]+',' ','g')
					   FROM pg_stat_statements AS pgstats
					   JOIN pg_database AS pgdb ON pgdb.oid = pgstats.dbid  
					  WHERE true 
					    AND datname = current_database() 
					    AND query ILIKE 'SELECT %' 
					    AND query NOT ILIKE '%from pg_%' 
					    AND query ILIKE '%where%'
					    AND min_exec_time >= i_min_exec);
						
	
		
	all_tables := ARRAY(SELECT relname as tablename
						  FROM pg_class C
						  LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace)
					     WHERE nspname NOT IN ('pg_catalog', 'information_schema') AND relkind='r' 
					     ORDER BY tablename);	

	FOREACH q IN ARRAY queries LOOP
	
		query_id :=	SUBSTRING(q,1,POSITION(',SELECT' in UPPER(q))-1);
		
		FOREACH tab in ARRAY all_tables LOOP			

			IF tab = ANY(string_to_array(q,' ')) THEN	
			
				-- get text after where clause without 'where' (+5)
				condition_clause :=	SUBSTRING(q,POSITION('WHERE' in UPPER(q))+5 ,LENGTH(q));	

				-- get all attributes of give table
				all_attributes :=  ARRAY(SELECT column_name
										   FROM information_schema.columns 
										  WHERE true 
											AND table_name LIKE tab);

				potential_fd := null;
				tab_attr_list := null;
				tab_attr_list = array_append(tab_attr_list,query_id);
				tab_attr_list = array_append(tab_attr_list,tab); 

				FOREACH attr IN ARRAY all_attributes LOOP	

					IF attr = ANY(string_to_array(condition_clause,' ')) THEN					

						potential_fd = array_append(potential_fd,attr);
						tab_attr_list = array_append(tab_attr_list,attr);

					END IF;	 -- IF ATTRIBUTE IN QUERY						
				END LOOP; -- ALL ATTRIBUTE	

				IF array_length(potential_fd, 1) > 0 THEN

					add_potential_fd := TRUE;

					IF array_length(output_string, 1) > 0 THEN
						
						FOREACH query_in_output in ARRAY output_string LOOP
						
							IF  string_to_array(query_in_output,',') =  tab_attr_list THEN 
							
							add_potential_fd := FALSE;
							
							END IF; -- new element is not existent in output_string
						END LOOP; -- for each element in output_string
					END IF; -- output_string > 0

					IF add_potential_fd THEN				
						output_string = array_append(output_string,array_to_string(tab_attr_list,','));					
					END IF;
				END IF;	-- Potential FD > 0
			END IF;	-- IF TABLE IN Q			
		END LOOP; --ALL TABLES		
	END LOOP; -- LOOP THROUGH QUERIES
	
	RETURN output_string;
END;
$BODY$;
