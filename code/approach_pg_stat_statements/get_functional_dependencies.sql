-------------------------------------------------------
/**
 * Author:		Kevin Ammann
 * Created:		12.05.2021
 * Project:		Functional Dependency Detection with PL/pgSQL
 * Version:		Initial Version
 * Function:    Check if FD exists among attributes per query. If argument 'create' is given, statistics will be created
 *              Return: String containing information as queri_id, table, attribute, FD and potential statistics

**/
-------------------------------------------------------

CREATE OR REPLACE FUNCTION public.get_functional_dependencies(IN min_exec numeric DEFAULT NULL, IN coef numeric DEFAULT NULL,IN create_stats text DEFAULT  NULL)
    RETURNS text
    LANGUAGE 'plpgsql'
    
AS $BODY$
DECLARE	

all_attr_pg_stats_statement text[];
q text;  			--query_result
q_id text;			--query_id
q_array text[]; 	--query_array {table_name,attribute_name1-n}
q_tab text; 		--query_table
q_attr text[]; 		--query_attributes 1-n
primary_key text;
all_permutations text[];	--all_attributes_permutation
permutation text;
deter text;
depend text;
query_to_check_fd text;
coefficient numeric;
i_coefficient numeric := COALESCE(coef,0.5);
i_min_exec numeric := min_exec;
fd text;
all_fd text[];
all_distinct_fd text[];
potential_distinct_fd text;
dist_fd text;
available boolean;

fd_string text;
distinct_fd_string text;
statistics_string text;
output_string text;
return_string text := '';

stats_name text;
BEGIN

	all_attr_pg_stats_statement := get_attributes(min_exec);
	--RAISE NOTICE 'all_attr_pg_stats_statement: %',all_attr_pg_stats_statement;
	
	IF create_stats ILIKE '%create%' THEN
		create_stats := true;
	ELSE
		create_stats := false;
	END IF;
	
	FOREACH q IN ARRAY(all_attr_pg_stats_statement) LOOP
		q_array := string_to_array(q,',');
		q_id := q_array[1];	
		q_tab := q_array[2];
		q_attr := q_array[3:];	
		
		output_string := '';
		output_string := E'\n'|| 'pg_stat_statements query_id: '|| E'\n' ||'  '|| q_id || E'\n';
		output_string := output_string || 'Table: ' || E'\n'||'  '|| q_tab || E'\n';
		output_string := output_string || 'Attributes: '|| E'\n' ||'  '|| array_to_string(q_attr,',') || E'\n';
	
	--RAISE NOTICE '%',output_string;
		--Get primary key
		SELECT ccu.column_name "referenced primary_column" into primary_key                   
		  FROM information_schema.table_constraints AS tc 
		  JOIN information_schema.key_column_usage AS kcu
			ON tc.constraint_name = kcu.constraint_name
		   AND tc.table_schema = kcu.table_schema						   
		  JOIN information_schema.constraint_column_usage AS ccu
			ON ccu.constraint_name = tc.constraint_name
		   AND ccu.table_schema = tc.table_schema
		   AND  ccu.table_name LIKE q_tab
		   AND constraint_type like 'PRIMARY KEY';
		  --RAISE NOTICE 'primary_key: %',primary_key;
		
		all_permutations := get_permutations(q_attr); 

		fd_string := '';
		distinct_fd_string := '';
		
		IF all_permutations IS NOT NULL THEN
			
			all_distinct_fd := null;
		
			FOREACH permutation IN ARRAY all_permutations LOOP
			--RAISE NOTICE 'permutation: %',permutation;

				deter := LEFT(permutation, POSITION('-' IN permutation)-1);
				--RAISE NOTICE 'deter: %',deter;

				depend := RIGHT(permutation, LENGTH(permutation) - POSITION('>'IN permutation));
				--RAISE NOTICE 'depend: %',depend;

				--IF deter like '%'||primary_key||'%' THEN
				IF primary_key = ANY(string_to_array(deter,',')) THEN
					continue;
				ELSE
					query_to_check_fd := 'SELECT '||deter||',COUNT(DISTINCT '|| depend ||') FROM '||q_tab|| ' GROUP BY ' ||deter;

					EXECUTE 		
						'with funct_dep_count as (' ||query_to_check_fd|| ')
							,temp_funct_attr(funct_attr) as
								(select count(*)  
								   from funct_dep_count
								  where count <= 1)
							,temp_total_attr(total_attr) as
								(select 
									CASE count(*) WHEN 0 THEN null ELSE count(*) END   
								   from funct_dep_count)
						 select (cast(funct_attr as numeric) / total_attr)
						   from temp_funct_attr ,temp_total_attr' into coefficient;


					IF coefficient >= i_coefficient THEN

						fd := deter ||'=>'||depend|| ': ' || round(coefficient,2) || E'\n';
						--RAISE NOTICE 'fd: %', fd;

						fd_string := fd_string ||'  '|| coalesce(fd,'') ;


						all_fd = array_append(all_fd,fd);

						potential_distinct_fd := deter||','||depend;
						--RAISE NOTICE 'q_id: %', q_id;
						--RAISE NOTICE 'potential_distinct_fd: %', potential_distinct_fd;
						
						available := false;
						IF array_length(all_distinct_fd,1) IS NULL THEN
							all_distinct_fd = array_append(all_distinct_fd,potential_distinct_fd);
							
							--RAISE NOTICE 'all_distinct_fd: %', all_distinct_fd;
						
						ELSE
						
							FOREACH dist_fd IN ARRAY all_distinct_fd LOOP

								--RAISE NOTICE 'dist_fd: %', dist_fd;

								IF (string_to_array(potential_distinct_fd,',') @> string_to_array(dist_fd,',') AND (array_length(string_to_array(potential_distinct_fd,','),1) = array_length(string_to_array(dist_fd,','),1))) THEN
									available := true;
									--RAISE NOTICE '% and % are the same ---> next',dist_fd,potential_distinct_fd;
									EXIT WHEN available; 
								END IF;
							END LOOP;

						END IF;
						
							IF NOT available THEN						
								--RAISE NOTICE '% and % are not the same ---> add',dist_fd,potential_distinct_fd;
								all_distinct_fd = array_append(all_distinct_fd,potential_distinct_fd);
								distinct_fd_string := distinct_fd_string ||'  '|| coalesce(potential_distinct_fd,'');
							
								IF create_stats THEN
									stats_name:= replace(potential_distinct_fd,',','_');
							
									EXECUTE 'CREATE STATISTICS IF NOT EXISTS '|| stats_name || ' (dependencies) ON ' || potential_distinct_fd|| ' FROM ' || quote_ident(q_tab);
							
								END IF;  --create statistics
							
							END IF; -- if potential_fd is NOT available
						
					END IF; --coefficient > give coef
				END IF;	--pk check	
			END LOOP; --permutation in all_permutation
		END IF; -- if all_permutation is not null
		output_string := output_string || 'Functional Dependencies: '|| E'\n';
		output_string := output_string ||  coalesce(fd_string,'No functional dependencies for this query!') || E'\n';
	
		IF create_stats THEN
			output_string := output_string || 'Statistics created for:' || E'\n';
			output_string := output_string || coalesce(distinct_fd_string,'No statistics for this query!') || E'\n';	
		ELSE
			output_string := output_string || 'Statistics would be created for:' || E'\n';
			output_string := output_string || coalesce(distinct_fd_string,'No statistics for this query!') || E'\n';
		END IF;
	
		RAISE NOTICE '%',output_string;
	
		return_string := return_string || coalesce(output_string,'');
	
		END LOOP; --Iterator through queryresults
	RETURN return_string;
END;
$BODY$;
